import pandas as pd
import json
import numpy as np
from transformers import BertTokenizer, BartTokenizer, MBart50TokenizerFast
from random import randint
from nltk import sent_tokenize
import random
import argparse
import os


def file_to_df(input_path):
    all_docs = []
    counter = 0
    with open(input_path, 'r', encoding='utf8') as f:
        for line in f:
            counter += 1
            if counter % 10000 == 0:
                print('Processing json: ', counter)
            line = json.loads(line)
            title = line.get('title') or ''
            abstract = line.get('abstract') or ''
            all_docs.append([title, abstract])

    df = pd.DataFrame(all_docs)
    df.columns = ["title", "abstract"]
    print(input_path, 'data size: ', df.shape)
    print('DF shape: ', df.shape)
    return df


def text_infilling(df, tokenizer, output, max_len, MASK_TOKEN):
    data = []
    for idx, row in df.iterrows():
        if idx % 500 == 0:
            print("Processing doc", idx)
        text = row['title'] + '. ' + row['abstract']
        text = " ".join(text.split())
        tokens = tokenizer.tokenize(text)[:max_len]
        text = tokenizer.convert_tokens_to_string(tokens)

        num_mask = int(0.15 * len(tokens))
        rand_positions = [randint(0, len(tokens) - 1) for _ in range(0, num_mask - 1)]
        for n in rand_positions:
            s = np.random.poisson(3, 1)[0]
            for i in range(s):
                if n + i < len(tokens):
                    tokens[n + i] = MASK_TOKEN
        i = 0
        while i < len(tokens) - 1:

            if tokens[i] == MASK_TOKEN and tokens[i] == tokens[i + 1]:
                del tokens[i]
            else:
                i = i + 1
        noised_text = tokenizer.convert_tokens_to_string(tokens)
        data.append((text, noised_text))
    df = pd.DataFrame(data)
    df.columns = ["title", "abstract"]
    df.to_csv(output + '_text_infilling.tsv', sep='\t', encoding='utf8')


def sentence_shuffling(df, tokenizer, output, max_len):
    data = []
    for idx, row in df.iterrows():
        if idx % 500 == 0:
            print("Processing doc", idx)
        text = row['title'] + '. ' + row['abstract']
        text = " ".join(text.split())
        tokens = tokenizer.tokenize(text)[:max_len]
        text = tokenizer.convert_tokens_to_string(tokens)
        sents = sent_tokenize(text)
        random.shuffle(sents)
        noised_text = " ".join(sents)
        data.append((text, noised_text))
    df = pd.DataFrame(data)
    df.columns = ["title", "abstract"]
    df.to_csv(output + '_sentence_shuffling.tsv', sep='\t', encoding='utf8')



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_path",
                        default='data/kptimes/kptimes_valid.json', type=str,
                        help="Path to input dataset.")
    parser.add_argument("--output_dir",
                        default='data/corrupted_data',
                        help="Path to folder that contains output_data")
    parser.add_argument("--model_type",
                        default='facebook/bart-base',
                        type=str,
                        help="Model identifier")
    parser.add_argument("--noising_type",
                        default='sentence-shuffling',
                        type=str,
                        help="text-infilling or sentence-shuffling")
    parser.add_argument("--language_code",
                        default="none",
                        type=str,
                        help='Language code needed by the MBART model, hr_HR for Croatian, et_EE for Estonian')
    parser.add_argument("--max_sequence_length", default=128, type=int)
    args = parser.parse_args()

    dataset = args.data_path

    df = file_to_df(dataset)
    output = dataset.split('.')[0].split('/')[-1]
    output = os.path.join(args.output_dir, output) + '_' + args.model_type.split('/')[-1]
    max_len = args.max_sequence_length

    assert args.noising_type in ['text-infilling', 'sentence-shuffling']

    if 'bert' in args.model_type:
        tokenizer = BertTokenizer.from_pretrained(args.model_type)
        MASK_TOKEN = '[MASK]'
    elif 'mbart' in args.model_type:
        tokenizer = MBart50TokenizerFast.from_pretrained(args.model_type, src_lang=args.language_code,
                                                             tgt_lang=args.language_code)
        MASK_TOKEN = '▁<mask>'
    elif 'bart' in args.model_type:
        tokenizer = BartTokenizer.from_pretrained(args.model_type)
        MASK_TOKEN = 'Ġ<mask>'


    if args.noising_type == 'text-infilling':
        print("Text infilling nosing")
        text_infilling(df, tokenizer, output, max_len, MASK_TOKEN)
    else:
        print("Sentence shuffling nosing")
        sentence_shuffling(df, tokenizer, output, max_len)

    print('Done, corrupted dataset written to', args.output_dir)
