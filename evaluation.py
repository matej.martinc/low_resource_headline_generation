import numpy as np
import torch
from sklearn.metrics.pairwise import cosine_similarity
from transformers import AutoModelForSequenceClassification, AutoTokenizer
from sentence_transformers import SentenceTransformer
import os
from rouge_score import rouge_scorer
from collections import defaultdict


def evaluation(result_dir, model_type, result_file, gpu, checkpoint):
    device = 'cuda:' + str(gpu)

    print()
    print('------------------------------')
    print('Calculating scores at step', checkpoint)

    with torch.no_grad():
        nli_model = AutoModelForSequenceClassification.from_pretrained("typeform/distilbert-base-uncased-mnli").to(device)
        nli_tokenizer = AutoTokenizer.from_pretrained("typeform/distilbert-base-uncased-mnli")
        nli_model.eval()
        if model_type in ['facebook/bart-base', 'bert-base-uncased']:
            sent_model = SentenceTransformer('sentence-transformers/paraphrase-MiniLM-L6-v2').to(device)
        else:
            sent_model = SentenceTransformer('sentence-transformers/paraphrase-xlm-r-multilingual-v1')

    print('& F1 & F2 & FL & SS & NLI \\\\')

    correct = []
    generated = []
    scores = defaultdict(list)

    ss = []  # semantic similarity
    nli = []  # textual entailment

    with open(os.path.join(result_dir, result_file), 'r', encoding='utf8') as f:
        idx = 0
        for row in f.readlines():
            idx += 1
            if idx == 1:
                continue
            original, headline = row.split('\t')
            original = " ".join(original.split())
            headline = " ".join(headline.split())
            correct.append(original.lower().split())
            headline = headline.lower()

            generated.append(headline.split())
            metric_dict = {"rouge-1": "rouge1", "rouge-2": "rouge2", "rouge-l": "rougeLsum"}
            for metric in metric_dict.keys():
                scorer = rouge_scorer.RougeScorer([metric_dict[metric]], use_stemmer=False)
                scorer_score = scorer.score(headline, original.lower())
                fscore = scorer_score[metric_dict[metric]].fmeasure
                scores[metric].append(fscore)

            nli_emb = nli_tokenizer.encode(headline, original.lower(), return_tensors='pt')
            with torch.no_grad():
                nli_logits = nli_model(nli_emb.to(device))[0].detach()
                sim = cosine_similarity(sent_model.encode(headline).reshape(1, -1), sent_model.encode(original.lower()).reshape(1, -1))

            # we throw away "neutral" (dim 1) and take the probability of "entailment" (0) as the probability of the label being true
            entail_contradiction_logits = nli_logits[:,::2]
            probs = entail_contradiction_logits.softmax(dim=1)
            prob_label_is_true = probs[:,0]
            nli.append(prob_label_is_true[0] * 100)
            ss.append(sim)

    print(result_file[:-4], '& ', ' & '.join([str(sum(scores['rouge-1'])/len(scores['rouge-1']))[:5], \
                                      str(sum(scores['rouge-2'])/len(scores['rouge-2']))[:5], \
                                      str(sum(scores['rouge-l'])/len(scores['rouge-l']))[:5], \
                                      str(np.mean(ss, 0)[0][0])[:5],\
                                      str(torch.mean(torch.stack(nli)).item())[:6]]), '\\\\')






