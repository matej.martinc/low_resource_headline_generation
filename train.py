from transformers import BertGenerationDecoder, BertGenerationEncoder, BertTokenizer, EncoderDecoderModel, AutoConfig, MBartForConditionalGeneration, MBart50TokenizerFast
from transformers import BartTokenizer, BartForConditionalGeneration, BartTokenizerFast
from transformers.models.bart import modeling_bart
from transformers import AdamW, get_linear_schedule_with_warmup
import argparse
import torch
import random
import os
from evaluation import evaluation
from dataset_load import *


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_type",
                        default='facebook/bart-base',
                        type=str,
                        help="Model identifier")
    parser.add_argument("--train_path",
                        default="data/kptimes/kptimes_valid.json",
                        type=str,
                        help="Path to input train dataset.")
    parser.add_argument("--test_path",
                        default="data/kptimes/kptimes_test.json",
                        type=str,
                        help="Path to input test dataset.")
    parser.add_argument("--model_transfer",
                        default='bart_base_text_infilling_kptimes.bin',
                        type=str,
                        help="Path to saved pretrained model.")
    parser.add_argument("--model_name",
                        default='bart_base_text_infilling_kptimes_fine_tuned.bin',
                        type=str,
                        help="Name of the output model.")
    parser.add_argument("--output_dir",
                        default='results',
                        type=str,
                        help="The output directory where the model predictions and checkpoints will be written.")
    parser.add_argument("--do_train",
                        action='store_true',
                        help="Whether to run training.")
    parser.add_argument("--do_eval",
                        action='store_true',
                        help="Whether to run eval on the dev set.")
    parser.add_argument("--transfer",
                        action='store_true',
                        help="Whether to use a pretrained model.")
    parser.add_argument("--language_code",
                        default="hr_HR",
                        type=str,
                        help='Language code needed by the MBART model, hr_HR for Croatian, et_EE for Estonian')


    ## Other parameters
    parser.add_argument("--max_seq_length",
                        default=128,
                        type=int,
                        help="The maximum total input sequence length after WordPiece tokenization. \n"
                             "Sequences longer than this will be truncated, and sequences shorter \n"
                             "than this will be padded.")
    parser.add_argument("--max_target_length",
                        default=30,
                        type=int,
                        help="The maximum total input sequence length after WordPiece tokenization. \n"
                             "Sequences longer than this will be truncated, and sequences shorter \n"
                             "than this will be padded.")
    parser.add_argument("--train_batch_size",
                        default=128,
                        type=int,
                        help="Total batch size for training.")
    parser.add_argument("--eval_batch_size",
                        default=12,
                        type=int,
                        help="Total batch size for eval.")
    parser.add_argument("--learning_rate",
                        default=2e-5,
                        type=float,
                        help="The initial learning rate for Adam.")
    parser.add_argument("--num_train_epochs",
                        default=10.0,
                        type=float,
                        help="Total number of training epochs to perform.")
    parser.add_argument("--no_cuda",
                        action='store_true',
                        help="Whether not to use CUDA when available")
    parser.add_argument('--seed',
                        type=int,
                        default=42,
                        help="random seed for initialization")
    parser.add_argument("--device",
                        default="cuda",
                        type=str,
                        help="Cuda device.")
    args = parser.parse_args()

    model_type = args.model_type
    model_name = args.model_name
    train_path = args.train_path
    test_path = args.test_path
    transfer_model = args.model_transfer

    device = torch.device(args.device if torch.cuda.is_available() and not args.no_cuda else "cpu")
    print("Training on device: ", device)
    n_gpu = torch.cuda.device_count()

    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if n_gpu > 0:
        torch.cuda.manual_seed_all(args.seed)
    os.makedirs(args.output_dir, exist_ok=True)
    if args.transfer:
        if '.' in model_name:
            model_n, model_suffix = model_name.split('.')
            model_name = model_n + '_transfer_fine_tuned.' + model_suffix
        else:
            model_name = model_name + '_transfer_fine_tuned'
    if not train_path.endswith('.tsv'):
        df_train = file_to_df(train_path)
        df_test = file_to_df(test_path)
    else:
        df_train = pd.read_csv(train_path, encoding='utf8', sep='\t')
        df_test = pd.read_csv(test_path, encoding='utf8', sep='\t')

    processor = DataProcessor()

    # create tokenizer...
    if 'bert' in model_type:
        tokenizer = BertTokenizer.from_pretrained(model_type)
    elif 'mbart' in model_type:
        tokenizer = MBart50TokenizerFast.from_pretrained(model_type, src_lang=args.language_code, tgt_lang=args.language_code)
    elif 'bart' in model_type:
        tokenizer = BartTokenizer.from_pretrained(model_type)

    if args.do_train:
        print('Training the model', model_type)

        train_guid, train_docs, train_labels = processor.get_train_examples(df_train)
        num_train_steps = int((len(train_docs) / args.train_batch_size) * args.num_train_epochs)

        t_total = num_train_steps
        if 'bert' in model_type:
            # use BERT's cls token as BOS token and sep token as EOS token
            bos, eos = tokenizer.convert_tokens_to_ids(['[CLS]', '[SEP]'])
            encoder = BertGenerationEncoder.from_pretrained(model_type, bos_token_id=bos, eos_token_id=eos)
            # add cross attention layers and use BERT's cls token as BOS token and sep token as EOS token
            decoder = BertGenerationDecoder.from_pretrained(model_type, add_cross_attention=True, is_decoder=True, bos_token_id=bos, eos_token_id=eos)
            model = EncoderDecoderModel(encoder=encoder, decoder=decoder)
        elif 'mbart' in model_type:
            model = MBartForConditionalGeneration.from_pretrained(model_type)
        elif 'bart' in model_type:
            model = BartForConditionalGeneration.from_pretrained(model_type)
        if args.transfer:
            print('Loading the enriched model', transfer_model)
            model_file_path = os.path.join(args.output_dir, transfer_model)
            model.load_state_dict(torch.load(model_file_path, map_location=args.device))
        model.to(device)
        optimizer = AdamW(model.parameters(), lr=args.learning_rate, correct_bias=False)
        scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=t_total)

        train_dataloader = create_data_loader(train_docs, train_labels, tokenizer, args.max_seq_length, args.max_target_length, args.train_batch_size, eval=False)

        for epoch_num in trange(int(args.num_train_epochs), desc="Epoch"):
            model.train()
            tr_loss = 0
            nb_tr_examples, nb_tr_steps = 0, 0
            for step, batch in enumerate(tqdm(train_dataloader, desc="Iteration")):
                input_ids = batch["input_ids"].to(device)
                targets = batch["targets"].to(device)

                if 'bert' in model_type: #or 'mbart' in model_type:
                    # bert
                    decoder_input_ids = targets.clone()
                    targets[targets == tokenizer.pad_token_id] = -100
                elif 'bart' in model_type:
                    #bart
                    decoder_start_token_id = model.config.decoder_start_token_id
                    pad_token_id = model.config.pad_token_id
                    decoder_input_ids = modeling_bart.shift_tokens_right(targets, pad_token_id, decoder_start_token_id)
                    targets[targets == pad_token_id] = -100
                loss = model(input_ids=input_ids, decoder_input_ids=decoder_input_ids, labels=targets)[0]

                loss.backward()
                tr_loss += loss.item()
                nb_tr_examples += input_ids.size(0)
                nb_tr_steps += 1
                if step % 100 == 0:
                    print("Step: ", step, "Loss: ", loss)

                torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
                optimizer.step()
                scheduler.step()
                optimizer.zero_grad()

            if epoch_num > 0 and epoch_num % 10 == 0:
                ## Save a trained model
                model_to_save = model.module if hasattr(model, 'module') else model  # Only save the model it-self
                output_model_file = os.path.join(args.output_dir, 'epoch_' + str(epoch_num) + '_' + model_name)
                torch.save(model_to_save.state_dict(), output_model_file)

        ## Save a trained model
        model_to_save = model.module if hasattr(model, 'module') else model  # Only save the model it-self
        output_model_file = os.path.join(args.output_dir, model_name)
        torch.save(model_to_save.state_dict(), output_model_file)
        torch.cuda.empty_cache()


    if args.do_eval:
        print('Evaluating')
        if '.' in model_name:
            model_n = model_name.split('.')[0]
        else:
            model_n = model_name
        output_write_file = open(os.path.join(args.output_dir, model_n + "_headlines.tsv"), 'w', encoding='utf8')
        output_write_file.write('True\tGenerated\n')
        if 'bert' in model_type:
            bos, eos = tokenizer.convert_tokens_to_ids(['[CLS]', '[SEP]'])
            encoder = BertGenerationEncoder.from_pretrained(model_type, bos_token_id=bos, eos_token_id=eos)
            # add cross attention layers and use BERT's cls token as BOS token and sep token as EOS token
            decoder = BertGenerationDecoder.from_pretrained(model_type, add_cross_attention=True, is_decoder=True,
                                                           bos_token_id=bos, eos_token_id=eos)
            model = EncoderDecoderModel(encoder=encoder, decoder=decoder)
        elif 'mbart' in model_type:
            model = MBartForConditionalGeneration.from_pretrained(model_type)
        elif 'bart' in model_type:
            model = BartForConditionalGeneration.from_pretrained(model_type)

        output_model_file = os.path.join(args.output_dir, model_name)
        model.load_state_dict(torch.load(output_model_file, map_location=args.device))
        model.to(device)

        eval_guid, eval_docs, eval_labels = processor.get_dev_examples(df_test)
        eval_dataloader = create_data_loader(eval_docs, eval_labels, tokenizer, args.max_seq_length, args.max_target_length, args.eval_batch_size, eval=True)

        model.eval()
        nb_eval_steps, nb_eval_examples = 0, 0

        count = 0

        for batch in tqdm(eval_dataloader, desc="Evaluating"):
            input_ids = batch["input_ids"].to(device)
            targets = batch["targets"].to(device)

            with torch.no_grad():
                outputs = model.generate(
                    input_ids,
                    max_length=args.max_target_length,
                    num_beams=5,
                    early_stopping=True,
                )

                for p_idx, prompt in enumerate(outputs):

                    generated = tokenizer.decode(prompt).replace('[PAD]', '').replace('[CLS]', '').replace('[SEP]', '').replace('[UNK]', '').\
                        replace('<pad>', '').replace('</s>', '').replace('<s>', '').replace('<unk>', '').replace("et_EE", '').replace("hr_HR", '').replace("en_XX", '').strip()
                    generated = " ".join(generated.split())
                    true = tokenizer.decode(targets[p_idx]).replace('[PAD]', '').replace('[CLS]', '').replace('[SEP]', '').replace('[UNK]', '').\
                        replace('<pad>', '').replace('</s>', '').replace('<s>', '').replace('<unk>', '').replace("et_EE", '').replace("hr_HR", '').replace("en_XX", '').strip()
                    true = " ".join(true.split())
                    if count % 500 == 0:
                        print("Batch: ", count, "Generated: ", generated)
                        print("Batch: ", count, "True: ", true)
                    output_write_file.write(f"{true}\t{generated}\n")
                print('----------------------------------------------------')
            count += 1
        output_write_file.close()
        evaluation(args.output_dir, args.model_type, model_n + "_headlines.tsv", 0, 'final')
    torch.cuda.empty_cache()
