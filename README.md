# Headline generation in a low resource setting

Code for experiments presented in the paper 'Effectiveness of Data Augmentation and Pretraining 
for Improving Neural Headline Generation in Low-Resource Settings' published in the LREC 2022 proceedings.


## Installation, documentation ##

Instructions for installation assume the usage of PyPI package manager.<br/>

Install dependencies if needed: pip install -r requirements.txt <br/>

#### Corrupt the data:<br/> 

Generate the corrupted data so that the models can be pretrained by restoring the uncorrupted text:<br/>

```
python noising.py  --data_path 'data/kptimes/kptimes_valid.json' --output_dir data/corrupted_data --model_type facebook/bart-base --noising_type sentence-shuffling --language_code none
```

**Arguments:**<br/>
**--data_path** Path to input dataset. <br/>
**--output_dir** Path to folder that contains output_data. <br/>
**--model_type** Huggingface model identifier, for English we employ 'facebook/bart-base' or 'bert-base-uncased' 
for Croatian 'EMBEDDIA/crosloengual-bert' or 'facebook/mbart-large-50' and for Estonian 'EMBEDDIA/finest-bert' or 'facebook/mbart-large-50'.<br/>
**--noising_typ** 'text-infilling' or 'sentence-shuffling' <br/>
**--model_type** BERT or GPT-2 <br/>
**--language_code** Language code needed by the MBART model, 'hr_HR' for Croatian, 'et_EE' for Estonian, 'none' for other models or languages.

**Outputs:**<br/>
A .tsv file containing the corrupted and uncorrupted input that can be fed to the pretraining.py script.


#### Data augmentation:<br/> 

Bert data augmentation:<br/>

```
python --input data/kptimes/kptimes_valid.json --output data/kptimes/kptimes_valid_bert.json --lang Choose language of the input dataset, can be either 'en' for English or 'hr' for Croatian or 'et' for Estonian
```

**Arguments:**<br/>
**--input** Path to input dataset to augment
**--output** Path to the output augmented dataset
**--lang** Choose language of the input dataset, can be either 'en' for English or 'hr' for Croatian or 'et' for Estonian 

**Outputs:**<br/>
An augmented dataset in the json format.

Other data augmentation:<br/>

TODO

#### Model pretraining :<br/> 

Pretrain the model on the .tsv file containing corrupted and uncorrupted data:<br/>

```
python pretraining.py --model_type facebook/bart-base --train_paths data/corrupted_data/kptimes_valid_bart-base_sentence_shuffling.tsv --output_dir results --model_name bart_base_sent_shuffling_kptimes.bin --do_train --do_eval --max_seq_length 128 --train_batch_size 128 --eval_batch_size 4 --num_train_epochs 10 --language_code none --device cuda
```

**Arguments:**<br/>
**--model_type** Huggingface model identifier, for English we employ 'facebook/bart-base' or 'bert-base-uncased' 
for Croatian 'EMBEDDIA/crosloengual-bert' or 'facebook/mbart-large-50' and for Estonian 'EMBEDDIA/finest-bert' or 'facebook/mbart-large-50'.<br/>
**--train_paths**  One or more paths to pretraining datasets for different tasks divided by comma.<br/>
**--combine** Add this flag if you want to do sequential pretraining on several datasets, 
i.e. if you train on more than one pretraining dataset. <br/>
**--model_name** Name of the output model.<br/>
**--output_dir** The output directory where the model checkpoints will be written. <br/>
**--do_train** Whether to run training. <br/>
**--do_eval** make the model restore couple of example text in the development set.
**--language_code** Language code needed by the MBART model, 'hr_HR' for Croatian, 'et_EE' for Estonian, 'none' for other models or languages.


**Outputs:**<br/>
The script saves a final pretrained model in the --output_dir


#### Model training:<br/>

Train a model on an augmented or non-augmented dataset, without loading previously pretrained model:<br/>

```
python train.py --model_type facebook/bart-base --train_path data/kptimes/kptimes_valid.json --test_path data/kptimes/kptimes_test.json --model_name bart_base_baseline.bin --output_dir results --do_train --do_eval --max_seq_length 128 --max_target_length 30 --train_batch_size 128 --eval_batch_size 8 --device cuda --num_train_epochs 10 --language_code none
```

Train a pretrained model:<br/>

```
python train.py --model_type facebook/bart-base --train_path data/kptimes/kptimes_valid.json --test_path data/kptimes/kptimes_test.json --transfer --model_transfer bart_base_sent_shuffling_kptimes.bin --model_name bart_base_sent_shuffling_kptimes_fine_tuned.bin --output_dir results --do_train --do_eval --max_seq_length 128 --max_target_length 30 --train_batch_size 128 --eval_batch_size 8 --device cuda --num_train_epochs 10 --language_code none
```

**Arguments:**<br/>
**--model_type** Huggingface model identifier <br/>
**--train_path** Path to input train dataset. <br/>
**--test_path** Path to input train dataset. <br/>
**--transfer** Use this flag to use a saved pretrained model pretrained in the previous step.<br/>
**--model_transfer** Path to saved pretrained model
**--model_name** Name of the output model.<br/>
**--output_dir** The output directory where the model predictions and checkpoints will be written. <br/>
**--do_train** Whether to run training. <br/>
**--do_eval** make the model restore couple of example text in the development set.
**--language_code** Language code needed by the MBART model, 'hr_HR' for Croatian, 'et_EE' for Estonian, 'none' for other models or languages.


**Outputs:**<br/>
The script saves a final trained model and the headlines generated on the test set in the --output_dir.




    




