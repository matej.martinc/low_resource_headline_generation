import pandas as pd
import json
import numpy as np
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm, trange



def file_to_df(input_path):
    all_docs = []
    counter = 0
    with open(input_path, 'r', encoding='utf8') as f:
        for line in f:
            counter += 1
            if counter % 10000 == 0:
                print('Processing json: ', counter)
            line = json.loads(line)
            title = line.get('title') or ''
            abstract = line.get('abstract') or ''
            all_docs.append([title, abstract])

    df = pd.DataFrame(all_docs)
    df.columns = ["title", "abstract"]
    print(input_path, 'data size: ', df.shape)
    print('DF shape: ', df.shape)
    return df


class Dataset(Dataset):
  def __init__(self, docs, targets, tokenizer, max_len, max_target_len, eval):
    self.docs = docs
    self.targets = targets
    self.tokenizer = tokenizer
    self.max_len = max_len
    self.max_target_len = max_target_len
    self.eval = eval

  def __len__(self):
    return len(self.docs)

  def __getitem__(self, item):
    doc = str(self.docs[item])
    target = self.targets[item]
    encoding = self.tokenizer.encode_plus(
      doc,
      add_special_tokens=True,
      truncation = True,
      max_length=self.max_len,
      return_token_type_ids=False,
      pad_to_max_length=True,
      return_attention_mask=True,
      return_tensors='pt',
    )

    target_encoding = self.tokenizer.encode_plus(
      target,
      add_special_tokens=True,
      truncation = True,
      max_length=self.max_target_len,
      return_token_type_ids=False,
      pad_to_max_length=True,
      return_attention_mask=True,
      return_tensors='pt',
    )

    return {
      'doc': doc,
      'input_ids': encoding['input_ids'].flatten(),
      'attention_mask': encoding['attention_mask'].flatten(),
      'attention_mask_target': target_encoding['attention_mask'].flatten(),
      'targets': target_encoding['input_ids'].flatten()
    }

def create_data_loader(docs, labels, tokenizer, max_len, max_target_len, batch_size, eval):
  ds = Dataset(
      docs=np.array(docs),
      targets=np.array(labels),
      tokenizer=tokenizer,
      max_len=max_len,
      max_target_len=max_target_len,
      eval=eval
  )
  return DataLoader(
    ds,
    batch_size=batch_size,
    num_workers=0
  )

class DataProcessor(object):

    def get_train_examples(self, data):
        """See base class."""
        return self._create_examples(data, "train")

    def get_dev_examples(self, data):
        """See base class."""
        return self._create_examples(data, "test")

    def _create_examples(self, lines, set_type):
        """Creates examples for the training and dev sets."""
        docs = []
        labels = []
        for idx, row in lines.iterrows():
            guid = "%s-%s" % (set_type, idx)
            text = row['abstract'].lower()
            label = row['title'].lower()
            docs.append(text)
            labels.append(label)
        return guid, docs, labels
