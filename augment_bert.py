import torch
from transformers import BertTokenizer, BertForMaskedLM
import pandas as pd
import argparse
import json
import torch.nn.functional as F


def file_to_df(input_path):
    all_docs = []
    counter = 0
    with open(input_path, 'r', encoding='utf8') as f:
        for line in f:
            counter += 1
            if counter % 10000 == 0:
                print('Processing json: ', counter)
            line = json.loads(line)
            title = line.get('title') or ''
            abstract = line.get('abstract') or ''
            all_docs.append([title, abstract])

    df = pd.DataFrame(all_docs)
    df.columns = ["title", "abstract"]
    print(input_path, 'data size: ', df.shape)
    print('DF shape: ', df.shape)
    return df


def enrich(input_file, output_path, lang):
    # Load pre-trained model tokenizer (vocabulary)
    print("Language", lang)
    if lang == 'en':
        tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    elif lang == 'hr':
        tokenizer = BertTokenizer.from_pretrained('EMBEDDIA/crosloengual-bert')
    elif lang == 'et':
        tokenizer = BertTokenizer.from_pretrained('EMBEDDIA/finest-bert')

    # Load pre-trained model (weights)
    if lang == 'en':
        model = BertForMaskedLM.from_pretrained('bert-base-uncased')
    elif lang == 'hr':
        model = BertForMaskedLM.from_pretrained('EMBEDDIA/crosloengual-bert')
    elif lang == 'et':
        model = BertForMaskedLM.from_pretrained('EMBEDDIA/finest-bert')
    model.cuda()
    model.eval()

    df_data = file_to_df(input_file)

    print("Data shape before preprocessing:", df_data.shape)
    document_counter = 0

    with open(output_path, 'w', encoding='utf8') as f:
        for index, row in df_data.iterrows():
            document_counter += 1
            title = row['title']
            abstract = row['abstract']
            f.write(json.dumps({'title': title, 'abstract': abstract}) + '\n')
            for i in range(5):
                tokens = tokenizer.tokenize(abstract)
                input_ids = torch.tensor(tokenizer.convert_tokens_to_ids(tokens), dtype=torch.long)
                masked_indices = torch.bernoulli(torch.full(input_ids.shape, 0.2)).bool()
                input_ids[masked_indices] = tokenizer.mask_token_id
                masked_seq = tokenizer.convert_ids_to_tokens(input_ids)
                masked_seq = tokenizer.convert_tokens_to_string(masked_seq)
                encoding = tokenizer.encode_plus(
                    masked_seq,
                    add_special_tokens=True,
                    truncation = True,
                    max_length=512,
                    return_token_type_ids=False,
                    return_attention_mask=True,
                    return_tensors='pt',
                )
                input_ids =  encoding['input_ids'].cuda()
                logits = model(input_ids)[0].squeeze()
                preds = F.softmax(logits, dim=-1)
                preds = torch.argmax(preds, dim=-1)
                result = tokenizer.convert_ids_to_tokens(preds)[1:-1]
                result = tokenizer.convert_tokens_to_string(result)
                result = " ".join(result.split())
                f.write(json.dumps({'title':title, 'abstract':result}) + '\n')
            if document_counter % 100 == 0:
                print('Enriching doc: ', document_counter)


if __name__ == '__main__':
    argparser = argparse.ArgumentParser(description='LM enrichment')
    argparser.add_argument('--input', type=str,
                           default='data/kptimes/kptimes_valid.json',
                           help='Choose dataset input file')
    argparser.add_argument('--output', type=str,
                           default="data/kptimes/kptimes_valid_bert.json",
                           help='Choose path of the output augmented dataset')
    argparser.add_argument('--lang', type=str,
                           default="en",
                           help="Choose language of the input dataset, can be either 'en' for English or 'hr' for Croatian or 'et' for Estonian")
    args = argparser.parse_args()

    assert args.lang in ['en', 'hr', 'et']
    enrich(args.input, args.output, lang=args.lang)
