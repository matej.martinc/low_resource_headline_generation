import os, sys
import nltk, gensim, json
from textaugment import Wordnet, EDA
import numpy as np
import random

class Word2vec:
    """
    A set of functions used to augment data.

    Typical usage: :: 
        >>> from textaugment import Word2vec
        >>> t = Word2vec('path/to/gensim/model'or 'gensim model itself')
        >>> t.augment('I love school')
        i adore school
    """
    
    def __init__(self, **kwargs):
        """
        A method to initialize a model on a given path.
        :type random_state: int, float, str, bytes, bytearray
        :param random_state: seed
        :type model: str or gensim.models.word2vec.Word2Vec
        :param model: The path to the model or the model itself.
        :type runs: int, optional
        :param runs: The number of times to augment a sentence. By default is 1.
        :type v: bool or optional
        :param v: Replace all the words if true. If false randomly replace words.
                Used in a Paper (https://www.cs.cmu.edu/~diyiy/docs/emnlp_wang_2015.pdf)
        :type p: float, optional
        :param p: The probability of success of an individual trial. (0.1<p<1.0), default is 0.5
        """

        # Set random state
        if 'random_state' in kwargs:
            self.random_state = kwargs['random_state']
            if isinstance(self.random_state, int):
                random.seed(self.random_state)
                np.random.seed(self.random_state)
            else:
                raise TypeError("random_state must have type int")

        # Set verbose to false if does not exists
        try:
            if kwargs['v']: 
                self.v = True
            else:
                self.v = False
        except KeyError:
            self.v = False

        try:
            if "p" in kwargs:
                if type(kwargs['p']) is not float:
                    raise TypeError("p represent probability of success and must be a float from 0.1 to 0.9. E.g p=0.5")
                elif type(kwargs['p']) is float:
                    self.p = kwargs['p']
            else:
                kwargs['p'] = 0.5  # Set default value
        except KeyError:
            raise

        # Error handling of given parameters
        try:
            if "runs" not in kwargs:
                kwargs["runs"] = 1  # Default value for runs
            elif type(kwargs["runs"]) is not int:
                raise TypeError("DataType for 'runs' must be an integer")
            if "model" not in kwargs:
                raise ValueError("Set the value of model. e.g model='path/to/model' or model itself")
            if type(kwargs['model']) not in [str,
                                             gensim.models.word2vec.Word2Vec,
                                             gensim.models.keyedvectors.Word2VecKeyedVectors,
                                            gensim.models.keyedvectors.FastTextKeyedVectors]:
                raise TypeError("Model path must be a string. "
                                "Or type of model must be a gensim.models.word2vec.Word2Vec or "
                                "gensim.models.keyedvectors.Word2VecKeyedVectors or "
                                "gensim.models.keyedvectors.FastTextKeyedVectors type. "
                                "To load a model use gensim.models.Word2Vec.load('path')")
        except (ValueError, TypeError):
            raise
        else:
            self.runs = kwargs["runs"] 
            self.model = kwargs["model"]
            self.p = kwargs["p"]
            try:
                if type(self.model) is str:
                    self.model = gensim.models.Word2Vec.load(self.model)
            except FileNotFoundError:
                print("Error: Model not found. Verify the path.\n")
                raise

        print("MOST SIMILAR")
        self.most_similar = {}


    def find_most_similar(self, word):
        if word not in self.most_similar:
            try:
                self.most_similar[word] = self.model.wv.most_similar(word)
            except KeyError as e:
                self.most_similar[word] = e
            
        if isinstance(self.most_similar[word], Exception):
            raise self.most_similar[word]
        else:
            return self.most_similar[word]
        
            

    def geometric(self, data):
        """
        Used to generate Geometric distribution.

        :type data: list
        :param data: Input data
        :rtype:   ndarray or scalar
        :return:  Drawn samples from the parameterized Geometric distribution.
        """

        data = np.array(data)
        first_trial = np.random.geometric(p=self.p, size=data.shape[0]) == 1  # Capture success after first trial
        return data[first_trial]


    
    
    def augment(self, data):
        """
        The method to replace words with similar words.
        
        :type data: str
        :param data: Input data
        :rtype:   str
        :return:  The augmented data
        """
        
        # Avoid nulls and other unsupported types
        if type(data) is not str: 
            raise TypeError("Only strings are supported")
        # Lower case and split
        data_tokens = data.lower().split()

        # Verbose = True then replace all the words.
        if self.v:
            for _ in range(self.runs):
                for index in range(len(data_tokens)):  # Index from 0 to length of data_tokens
                    try:
                        similar_words = [syn for syn, t in self.find_most_similar(data_tokens[index])]
                        r = random.randrange(len(similar_words))
                        data_tokens[index] = similar_words[r].lower()  # Replace with random synonym from 10 synonyms
                    except KeyError:
                        pass  # For words not in the word2vec model
        else:  # Randomly replace some words
            for _ in range(self.runs):
                data_tokens_idx = [[x, y] for (x, y) in enumerate(data_tokens)]  # Enumerate data
                words = self.geometric(data=data_tokens_idx).tolist()  # List of words indexed
                for w in words:
                    try:
                        similar_words_and_weights = [(syn, t) for syn, t in self.find_most_similar(w[1])]
                        similar_words = [word for word, t in similar_words_and_weights]
                        similar_words_weights = [t for word, t in similar_words_and_weights]
                        word = random.choices(similar_words, similar_words_weights, k=1)
                        data_tokens[int(w[0])] = word[0].lower()  # Replace with random synonym from 10 synonyms
                    except KeyError:
                        pass
            return " ".join(data_tokens)
        return " ".join(data_tokens)



def augment_w2v(text):
    return " ".join([w2v.augment(s) for s in nltk.sent_tokenize(text)]).replace("_`_", "`").replace("_", " ")

def augment_wordnet(text):
    return wn.augment(text)

def augment_eda(text):
    text = eda.synonym_replacement(text, n=50)
    text = eda.random_insertion(text, n=50)
    text = eda.random_swap(text, n=10)
    text = eda.random_deletion(text, p=0.1)
    return text

def augment_mix(text):
    return (augment_w2v(augment_eda(augment_w2v(augment_wordnet(text)))))

if __name__ == "__main__":
	model = gensim.models.KeyedVectors.load_word2vec_format("../GoogleNews-vectors-negative300.bin", binary=True)
	w2v = Word2vec(model=model, runs=5, v=False, p=0.3)
	wn = Wordnet(v=True, n=True, runs=50, p=0.3)
	eda = EDA()
	
	input_file = sys.argv[1]
	output_base = os.path.splitext(input_file)[0]
	out_wn = open(output_base + "_wordnet.json", 'w', encoding="utf8")
	out_w2v = open(output_base + "_w2v.json", 'w', encoding="utf8")
	out_eda = open(output_base + "_eda.json", 'w', encoding="utf8")
	out_mix = open(output_base + "_mix.json", 'w', encoding="utf8")
	
	f = open(input_file,'r', encoding="utf8")
	for line in f:
	    doc = json.loads(line)
	    print(doc["id"])
	    text = doc['abstract']
	    text = ' '.join(text.split()[:500])
	    title = doc['title']
	
	    original = json.dumps({"title":title,
	                           "abstract":text})
	    for f in [out_wn,  out_eda]: # out_w2v, out_mix
                print(original, file = f)
            
	
	    for i in range(5):
                print(json.dumps({"title":title,
                                  "abstract":augment_wordnet(text)}),
                      file=out_wn)
                print(json.dumps({"title":title,
                                  "abstract":augment_eda(text)}),
                      file=out_eda)
                print(json.dumps({"title":title,
                                  "abstract":augment_w2v(text)}),
                      file=out_w2v)
                print(json.dumps({"title":title,
	                          "abstract":augment_mix(text)}),
	              file=out_mix)

	for f in [out_wn, out_w2v, out_eda, out_mix]: f.close()
	
