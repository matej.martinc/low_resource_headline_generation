from transformers import BartTokenizer, BartForConditionalGeneration
from transformers import BertGenerationDecoder, BertGenerationEncoder, BertTokenizer, EncoderDecoderModel, \
    MBartForConditionalGeneration, MBart50TokenizerFast
from transformers import AdamW, get_linear_schedule_with_warmup
from transformers.models.bart import modeling_bart
import pandas as pd
import argparse
import torch
import numpy as np
import random
import os
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm, trange


class Dataset(Dataset):
  def __init__(self, docs, targets, tokenizer, max_len, eval):
    self.docs = docs
    self.targets = targets
    self.tokenizer = tokenizer
    self.max_len = max_len
    self.eval = eval

  def __len__(self):
    return len(self.docs)

  def __getitem__(self, item):
    doc = str(self.docs[item])
    target = self.targets[item]
    encoding = self.tokenizer.encode_plus(
      doc,
      add_special_tokens=True,
      truncation = True,
      max_length=self.max_len,
      return_token_type_ids=False,
      pad_to_max_length=True,
      return_attention_mask=True,
      return_tensors='pt',
    )

    target_encoding = self.tokenizer.encode_plus(
      target,
      add_special_tokens=True,
      truncation = True,
      max_length=self.max_len,
      return_token_type_ids=False,
      pad_to_max_length=True,
      return_attention_mask=True,
      return_tensors='pt',
    )

    return {
      'doc': doc,
      'input_ids': encoding['input_ids'].flatten(),
      'attention_mask': encoding['attention_mask'].flatten(),
      'attention_mask_target': target_encoding['attention_mask'].flatten(),
      'targets': target_encoding['input_ids'].flatten()
    }


def create_data_loader(docs, labels, tokenizer, max_len, batch_size, eval):
  ds = Dataset(
      docs=np.array(docs),
      targets=np.array(labels),
      tokenizer=tokenizer,
      max_len=max_len,
      eval=eval
  )
  return DataLoader(
    ds,
    batch_size=batch_size,
    num_workers=0
  )


class DataProcessor(object):

    def get_train_examples(self, data):
        """See base class."""
        return self._create_examples(data, "train")

    def get_dev_examples(self, data):
        """See base class."""
        return self._create_examples(data, "test")

    def _create_examples(self, lines, set_type):
        """Creates examples for the training and dev sets."""
        docs = []
        labels = []
        for idx, row in lines.iterrows():
            guid = "%s-%s" % (set_type, idx)
            text = row['abstract']
            label = row['title']
            docs.append(text)
            labels.append(label)

        return guid, docs, labels


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--model_type",
                        default='facebook/bart-base',
                        type=str,
                        help="Model identifier")
    parser.add_argument("--train_paths",
                        default="data/corrupted_data/kptimes_valid_bart-base_sentence_shuffling.tsv",
                        type=str,
                        help="Paths to pretraining datasets for different tasks divided by comma.")
    parser.add_argument("--combine",
                        action='store_true',
                        help="Sequential pretraining on several datasets")
    parser.add_argument("--model_name",
                        type=str,
                        help="Name of the output model.")
    parser.add_argument("--output_dir",
                        default='results',
                        type=str,
                        help="The output directory where the model predictions and checkpoints will be written.")
    parser.add_argument("--do_train",
                        action='store_true',
                        help="Whether to run training.")
    parser.add_argument("--do_eval",
                        action='store_true',
                        help="Whether to run eval on the dev set.")

    ## Other parameters
    parser.add_argument("--max_seq_length",
                        default=128,
                        type=int,
                        help="The maximum total input sequence length after WordPiece tokenization. \n"
                             "Sequences longer than this will be truncated, and sequences shorter \n"
                             "than this will be padded.")
    parser.add_argument("--train_batch_size",
                        default=128,
                        type=int,
                        help="Total batch size for training.")
    parser.add_argument("--eval_batch_size",
                        default=1,
                        type=int,
                        help="Total batch size for eval.")
    parser.add_argument("--learning_rate",
                        default=2e-5,
                        type=float,
                        help="The initial learning rate for Adam.")
    parser.add_argument("--num_train_epochs",
                        default=10.0,
                        type=float,
                        help="Total number of training epochs to perform for each train task.")
    parser.add_argument("--no_cuda",
                        action='store_true',
                        help="Whether not to use CUDA when available")
    parser.add_argument('--seed',
                        type=int,
                        default=42,
                        help="random seed for initialization")
    parser.add_argument("--device",
                        default="cuda",
                        type=str,
                        help="Cuda device.")
    parser.add_argument("--language_code",
                        default="hr_HR",
                        type=str,
                        help='Language code needed by the MBART model, hr_HR for Croatian, et_EE for Estonian')

    args = parser.parse_args()

    device = torch.device(args.device if torch.cuda.is_available() and not args.no_cuda else "cpu")
    n_gpu = torch.cuda.device_count()

    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    if n_gpu > 0:
        torch.cuda.manual_seed_all(args.seed)
    os.makedirs(args.output_dir, exist_ok=True)

    pretraining_datasets = args.train_paths.split(',')

    for d_idx, predataset in enumerate(pretraining_datasets):
        df = pd.read_csv(predataset, encoding='utf8', sep='\t')
        df_idx = int(len(df) * 0.9)
        df_train = df[:df_idx]
        df_test = df[df_idx:]
        print('Enriching on train of shape', df_train.shape, 'and test of shape', df_test.shape)

        processor = DataProcessor()

        model_name = args.model_name

        output_model_file = os.path.join(args.output_dir, model_name)
        max_target_len = args.max_seq_length

        # create tokenizer...
        if 'bert' in args.model_type:
            tokenizer = BertTokenizer.from_pretrained(args.model_type)
        elif 'mbart' in args.model_type:
            tokenizer = MBart50TokenizerFast.from_pretrained(args.model_type, src_lang=args.language_code, tgt_lang=args.language_code)
        elif 'bart' in args.model_type:
            tokenizer = BartTokenizer.from_pretrained(args.model_type)

        if args.do_train:
            print('Pretraining the model on', predataset)

            train_guid, train_docs, train_labels = processor.get_train_examples(df_train)
            num_train_steps = int((len(train_docs) / args.train_batch_size) * args.num_train_epochs)

            t_total = num_train_steps
            if 'bert' in args.model_type:
                # use BERT's cls token as BOS token and sep token as EOS token
                bos, eos = tokenizer.convert_tokens_to_ids(['[CLS]', '[SEP]'])
                encoder = BertGenerationEncoder.from_pretrained(args.model_type, bos_token_id=bos, eos_token_id=eos)
                # add cross attention layers and use BERT's cls token as BOS token and sep token as EOS token
                decoder = BertGenerationDecoder.from_pretrained(args.model_type, add_cross_attention=True, is_decoder=True, bos_token_id=bos, eos_token_id=eos)
                model = EncoderDecoderModel(encoder=encoder, decoder=decoder)
            elif 'mbart' in args.model_type:
                model = MBartForConditionalGeneration.from_pretrained(args.model_type)
            elif 'bart' in args.model_type:
                model = BartForConditionalGeneration.from_pretrained(args.model_type)

            if args.combine:
                if d_idx > 0:
                    print('Loading the enriched model', model_name)
                    model.load_state_dict(torch.load(output_model_file, map_location=args.device))
                else:
                    print('Initializing the model of type', args.model_type)
            else:
                print('Initializing model', model_name, 'of type', args.model_type)

            print('Target length', max_target_len)

            model.to(device)
            optimizer = AdamW(model.parameters(), lr=args.learning_rate, correct_bias=False)
            scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=t_total)

            train_dataloader = create_data_loader(train_docs, train_labels, tokenizer, args.max_seq_length, args.train_batch_size, eval=False)
            for epoch_num in trange(int(args.num_train_epochs), desc="Epoch"):
                model.train()
                tr_loss = 0
                nb_tr_examples, nb_tr_steps = 0, 0
                for step, batch in enumerate(tqdm(train_dataloader, desc="Iteration")):
                    input_ids = batch["input_ids"].to(device)
                    targets = batch["targets"].to(device)

                    if 'bert' in args.model_type:
                        # bert
                        decoder_input_ids = targets.clone()
                        targets[targets == tokenizer.pad_token_id] = -100
                    if 'bart' in args.model_type:
                        #bart
                        decoder_start_token_id = model.config.decoder_start_token_id
                        pad_token_id = model.config.pad_token_id
                        targets[targets == pad_token_id] = -100
                        decoder_input_ids = modeling_bart.shift_tokens_right(targets, pad_token_id, decoder_start_token_id)

                    loss = model(input_ids=input_ids, decoder_input_ids=decoder_input_ids, labels=targets)[0]

                    loss.backward()
                    tr_loss += loss.item()
                    nb_tr_examples += input_ids.size(0)
                    nb_tr_steps += 1
                    if step % 100 == 0:
                        print("Step: ", step, "Train loss: ", tr_loss/nb_tr_steps)

                    torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
                    optimizer.step()
                    scheduler.step()
                    optimizer.zero_grad()

            ## Save a trained model
            model_to_save = model.module if hasattr(model, 'module') else model  # Only save the model it-self
            torch.save(model_to_save.state_dict(), output_model_file)


        if args.do_eval:
            print('Generating 20 examples of restored text by model', model_name)
            if 'bert' in args.model_type:
                bos, eos = tokenizer.convert_tokens_to_ids(['[CLS]', '[SEP]'])
                encoder = BertGenerationEncoder.from_pretrained(args.model_type, bos_token_id=bos, eos_token_id=eos)
                # add cross attention layers and use BERT's cls token as BOS token and sep token as EOS token
                decoder = BertGenerationDecoder.from_pretrained(args.model_type, add_cross_attention=True, is_decoder=True, bos_token_id=bos, eos_token_id=eos)
                model = EncoderDecoderModel(encoder=encoder, decoder=decoder)
            elif 'mbart' in args.model_type:
                model = MBartForConditionalGeneration.from_pretrained(args.model_type)
            elif 'bart' in args.model_type:
                model = BartForConditionalGeneration.from_pretrained(args.model_type)
            output_model_file = os.path.join(args.output_dir, model_name)
            model.load_state_dict(torch.load(output_model_file, map_location=args.device))
            model.to(device)
            eval_guid, eval_docs, eval_labels = processor.get_dev_examples(df_test)
            eval_dataloader = create_data_loader(eval_docs, eval_labels, tokenizer, args.max_seq_length, args.eval_batch_size, eval=True)

            model.eval()
            nb_eval_steps, nb_eval_examples = 0, 0

            predicted_all = []
            true_all = []
            count = 0

            for batch in eval_dataloader:

                input_ids = batch["input_ids"].to(device)
                targets = batch["targets"].to(device)

                with torch.no_grad():
                    outputs = model.generate(
                        input_ids,
                        max_length=max_target_len,
                        num_beams=5,
                        early_stopping=True
                    )
                    print('Generated example ', count + 1)
                    print("Generated: ", tokenizer.decode(outputs[0]).replace('[PAD]', '').replace('<pad>', ''))
                    print()
                    print("True: ", tokenizer.decode(targets[0]).replace('[PAD]', '').replace('<pad>', ''))
                    print('----------------------------------------------------')
                count += 1
                if count == 20:
                    break
            print('Done generating 20 examples')
        torch.cuda.empty_cache()
